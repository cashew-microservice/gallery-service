package com.cashew.gallery.controllers;

import com.cashew.gallery.entities.Gallery;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/gallery")
public class GalleryController {

    @RequestMapping
    public String normalUser() {
        return "Anyone who is authenticated can access";
    }
    @RequestMapping("/admin")
    public String homeAdmin() {
        return "Only admin roles who is authenticated can access";
    }
}
