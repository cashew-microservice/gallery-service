package com.cashew.gallery.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImageV2 {
    private int id;
    private String name;
    private String url;
    private String version;

}
