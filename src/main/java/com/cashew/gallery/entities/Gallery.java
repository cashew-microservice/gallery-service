package com.cashew.gallery.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Gallery {
    private int id;
    private String name;
    private String url;
    private String version;
}
